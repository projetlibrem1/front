import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {StoryService} from '../../../tools/services/story.service';
import {Story} from '../../../tools/classes/Story';
import {SuccessHandler} from '../../../tools/handlers/success.handler';
import {ErrorHandler} from '../../../tools/handlers/error.handler';
import {Theme} from '../../../tools/classes/Theme';
import {ThemeService} from '../../../tools/services/theme.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../tools/services/authentication.service';

@Component({
  selector: 'app-story-from',
  templateUrl: './story-from.component.html',
  styleUrls: ['./story-from.component.css']
})
export class StoryFromComponent implements OnInit {
  storyForm: FormGroup;
  @Input() story?: Story = new Story();
  themes: Theme[];
  status = [{text: 'Visible publiquement', value: 'active'}, {text: 'Invisible', value: 'doing'}];


  constructor(
    private storyService: StoryService,
    private themeService: ThemeService,
    private authService: AuthenticationService,
    private successHandler: SuccessHandler,
    private errorHandler: ErrorHandler,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.themeService.getAll().subscribe(data => {
      this.themes = data.themes;
    });
    this.storyForm = new FormGroup({
      title: new FormControl(this.story.title, [Validators.required]),
      theme: new FormControl(this.story.title, [Validators.required]),
      status: new FormControl(this.story.status, [Validators.required])
    });
  }

  save(): void  {
    this.story.title = this.storyForm.get('title').value;
    this.story.theme_id = this.storyForm.get('theme').value;
    this.story.userId = this.authService.getCurrent().id;
    this.story.status = this.storyForm.get('status').value;
    console.log(this.story);
    this.storyService.save(this.story).subscribe(story => {
      console.log(story);
      this.successHandler.handleSuccess('Et hop, une nouvelle histoire !');
      this.router.navigate(['/creation/new-chapter', {id: story.id}]);
    }, err => this.errorHandler.handleError('Oups, quelque chose n\'a pas fonctionné'));
  }

}
