import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoryFromComponent } from './story-from.component';

describe('StoryFromComponent', () => {
  let component: StoryFromComponent;
  let fixture: ComponentFixture<StoryFromComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoryFromComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoryFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
