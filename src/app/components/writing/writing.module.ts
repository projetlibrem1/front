import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WritingRoutingModule } from './writing-routing.module';
import { WritingComponent } from './writing.component';
import { ResumeComponent } from './resume/resume.component';
import { StoryFromComponent } from './story-from/story-from.component';
import { ChapterFormComponent } from './chapter-form/chapter-form.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import { CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {MatInputModule} from '@angular/material/input';
import {MatOptionModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import { StoryDetailsComponent } from './story-details/story-details.component';
import { PartFormComponent } from './part-form/part-form.component';

@NgModule({
  declarations: [WritingComponent, ResumeComponent, StoryFromComponent, ChapterFormComponent, StoryDetailsComponent, PartFormComponent],
  imports: [
    CommonModule,
    WritingRoutingModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    CKEditorModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule

  ]
})
export class WritingModule { }
