import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Chapter} from '../../../tools/classes/Chapter';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ChapterService} from '../../../tools/services/chapter.service';
import {SuccessHandler} from '../../../tools/handlers/success.handler';
import {ErrorHandler} from '../../../tools/handlers/error.handler';
import {ActivatedRoute, Router} from '@angular/router';
import {Story} from '../../../tools/classes/Story';
import {StoryService} from '../../../tools/services/story.service';
import {User} from '../../../tools/classes/User';
import {AuthenticationService} from '../../../tools/services/authentication.service';


@Component({
  selector: 'app-chapter-form',
  templateUrl: './chapter-form.component.html',
  styleUrls: ['./chapter-form.component.css']
})
export class ChapterFormComponent implements OnInit {
  chapterForm: FormGroup;
  chapter: Chapter = new Chapter();
  public Editor = ClassicEditor;
  story: Story;
  user: User;
  isUpdate = false;
  status = [{text: 'Visible publiquement', value: 'active'}, {text: 'Invisible', value: 'doing'}];

  constructor(
    private chapterService: ChapterService,
    private storyService: StoryService,
    private authService: AuthenticationService,
    private successHandler: SuccessHandler,
    private errorHandler: ErrorHandler,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.paramMap.subscribe(p => {
      if (p.get('id')) {
        this.storyService.getById(Number(p.get('id'))).subscribe( data => this.story = data);
      } else {
        this.router.navigate(['/404']);
      }
      if (p.get('chapter')) {
        this.chapterService.getById(Number(p.get('chapter'))).subscribe(chapter => {
          console.log(chapter);
          this.chapter = chapter.chapter;
          console.log(this.chapter);
          this.isUpdate = true;
          this.ngOnInit();
        });
      }
    });
  }

  ngOnInit(): void {
    console.log('coucou from init');
    console.log('name ', this.chapter.chapter_name);
    this.chapterForm = new FormGroup({
      title: new FormControl( this.chapter.chapter_name),
      text: new FormControl(this.chapter.text),
      status: new FormControl(this.chapter.status)
    });
  }

  save(): void {
    this.chapter.chapter_name = this.chapterForm.get('title').value;
    this.chapter.text = this.chapterForm.get('text').value;
    this.chapter.status = this.chapterForm.get('status').value;
    this.chapter.story_id = this.story.id;
    console.log(this.chapter);

    // Bloquer l'utilisateur si il veut mettre <script> ou <?php>....
    if (this.chapter.text.includes('&lt;script&gt;')) {
      this.errorHandler.handleError('Oups, quelque chose n\'a pas fonctionner 😠');
      console.log('error : tentative d\'ajout de script dans le texte');
    } else if (this.chapter.text.includes('&lt;?php')) {
      this.errorHandler.handleError('Oups, quelque chose n\'a pas fonctionner 😠');
      console.log('error : tentative d\'ajout de php dans le texte');
    } else {
      if (this.isUpdate) {
        this.chapterService.update(this.chapter).subscribe(_ => {
          this.successHandler.handleSuccess('Chapitre modifié avec succés ! Congrat !');
          this.router.navigate(['/creation/story', {id: this.story.id}]);
        }, err => this.errorHandler.handleError('Oups, quelque chose n\'a pas fonctionné'));
      } else {
        this.chapterService.create(this.chapter).subscribe(_ => {
          this.successHandler.handleSuccess('Et hop, un nouveau chapitre ! Bravo !');
          this.router.navigate(['/creation/story', {id: this.story.id}]);
        }, err => this.errorHandler.handleError('Oups, quelque chose n\'a pas fonctionné'));
      }
    }
  }
}
