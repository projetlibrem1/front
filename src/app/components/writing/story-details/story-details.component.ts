import {Component, OnInit} from '@angular/core';
import {ChapterService} from '../../../tools/services/chapter.service';
import {StoryService} from '../../../tools/services/story.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Story} from '../../../tools/classes/Story';
import {Chapter} from '../../../tools/classes/Chapter';

@Component({
  selector: 'app-story-details',
  templateUrl: './story-details.component.html',
  styleUrls: ['./story-details.component.css']
})
export class StoryDetailsComponent implements OnInit {
  story: Story;
  chapters: Chapter[];

  constructor(
    private chapterService: ChapterService,
    private storyService: StoryService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.paramMap.subscribe(p => {
      if (p.get('id')) {
        this.storyService.getById(Number(p.get('id'))).subscribe( data => {
          this.story = data;
          this.chapterService.getAllByStory(this.story).subscribe(c => {
            this.chapters = c.chapter;
            console.log(c);
          });
        });
      } else {
        this.router.navigate(['/404']);
      }
    });
  }

  ngOnInit(): void {
  }

}
