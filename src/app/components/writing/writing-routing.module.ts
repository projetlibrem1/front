import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WritingModule} from './writing.module';
import {WritingComponent} from './writing.component';
import {ChapterFormComponent} from './chapter-form/chapter-form.component';
import {ResumeComponent} from './resume/resume.component';
import {StoryFromComponent} from './story-from/story-from.component';
import {StoryDetailsComponent} from './story-details/story-details.component';

const routes: Routes = [
  {
    path: '',
    component: WritingComponent,
    children: [

      { path: '', component: ResumeComponent},
      { path: 'new-chapter', component: ChapterFormComponent},
      { path: 'new-story', component: StoryFromComponent},
      {path: 'story', component: StoryDetailsComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WritingRoutingModule { }
