import { Component, OnInit } from '@angular/core';
import {Story} from '../../../tools/classes/Story';
import {StoryService} from '../../../tools/services/story.service';
import {AuthenticationService} from '../../../tools/services/authentication.service';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css']
})
export class ResumeComponent implements OnInit {
  stories: Story[];

  constructor(
    private storyService: StoryService,
    private authService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.storyService.getAllByUser(this.authService.getCurrent().id).subscribe(data => this.stories = data.stories);
  }

}
