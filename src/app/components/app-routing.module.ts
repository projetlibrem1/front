import {NgModule} from '@angular/core';
import {NotFoundComponent} from './body/not-found/not-found.component';
import {RouterModule} from '@angular/router';
import {StoriesComponent} from './reading/stories/stories.component';

export const Routes = [
  {
    path: '',
    loadChildren: () => import('./reading/reading.module').then(m => m.ReadingModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    // TODO : user guard
    path: 'creation',
    loadChildren: () => import('./writing/writing.module').then(m => m.WritingModule)
  },
  {
    // TODO : user guard
    path: 'profil',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
  },
  {
    // TODO : admin guard
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: 'reading',
    loadChildren: () => import('./reading/reading.module').then(m => m.ReadingModule)
  },
  {path: '**', component: NotFoundComponent},
  {path: 'stories', component: StoriesComponent}
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    RouterModule.forRoot(Routes, {enableTracing: false})
  ]
})
export class AppRoutingModule { }
