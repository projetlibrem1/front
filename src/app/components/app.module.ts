import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {NotFoundComponent} from './body/not-found/not-found.component';
import {HeaderComponent} from './body/header/header.component';
import {AppRoutingModule} from './app-routing.module';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CustomInterceptor} from '../tools/interceptors/custom.interceptor';
import {SuccessHandler} from '../tools/handlers/success.handler';
import {ErrorHandler} from '../tools/handlers/error.handler';
import {StoriesGridComponent} from './body/stories-grid/stories-grid.component';
import {FooterComponent} from './body/footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NotFoundComponent,
    StoriesGridComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    MatInputModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatTooltipModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: CustomInterceptor, multi: true},
    SuccessHandler,
    ErrorHandler
  ],
  exports: [
    StoriesGridComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
