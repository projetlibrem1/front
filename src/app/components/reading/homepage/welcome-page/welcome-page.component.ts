import {Component, OnInit} from '@angular/core';
import {StoryService} from '../../../../tools/services/story.service';
import {AuthenticationService} from '../../../../tools/services/authentication.service';
import {Theme} from '../../../../tools/classes/Theme';
import {ThemeService} from '../../../../tools/services/theme.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.css']
})
export class WelcomePageComponent implements OnInit {
  stories;

  themes: Theme[];

  constructor(
    private storiesService: StoryService,
    private authService: AuthenticationService,
    private themeService: ThemeService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.paramMap.subscribe(params => params.get('id'));
  }

  ngOnInit(): void {
    this.getAll();
  }

  isConnected = (): boolean => this.authService.isConnected();

  getAll(): void {
    this.storiesService.getAllActive().subscribe(data => this.stories = data.stories, err => console.log(err));
  }
}
