import { Component, OnInit } from '@angular/core';
import {ThemeService} from '../../../../tools/services/theme.service';
import {Theme} from '../../../../tools/classes/Theme';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  themes: Theme[];
  constructor(
    private themeService: ThemeService
  ) { }

  ngOnInit(): void {
    this.themeService.getAll().subscribe(data => this.themes = data.themes);
  }

}
