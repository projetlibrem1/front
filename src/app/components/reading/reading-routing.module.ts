import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReadingComponent} from './reading.component';
import {WelcomePageComponent} from './homepage/welcome-page/welcome-page.component';
import {StoryComponent} from './stories/story/story.component';
import {ChapterComponent} from './stories/story/chapter/chapter.component';

const routes: Routes = [
  {
    path: '',
    component: ReadingComponent,
    children: [
      {path: '', component: WelcomePageComponent},
      {path: 'story', component: StoryComponent},
      {path: 'chapter', component: ChapterComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReadingRoutingModule { }
