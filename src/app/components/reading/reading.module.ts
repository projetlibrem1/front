import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReadingRoutingModule} from './reading-routing.module';
import {ReadingComponent} from './reading.component';
import {WelcomePageComponent} from './homepage/welcome-page/welcome-page.component';
import {StoriesComponent} from './stories/stories.component';
import {BannerComponent} from './homepage/banner/banner.component';
import {HomepageComponent} from './homepage/homepage.component';
import {CategoriesComponent} from './homepage/categories/categories.component';
import {StoryComponent} from './stories/story/story.component';
import {MatIconModule} from '@angular/material/icon';
import {ChapterComponent} from './stories/story/chapter/chapter.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [
    ReadingComponent,
    WelcomePageComponent,
    StoriesComponent,
    HomepageComponent,
    BannerComponent,
    CategoriesComponent,
    StoryComponent,
    ChapterComponent,
  ],
  imports: [
    CommonModule,
    ReadingRoutingModule,
    MatIconModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
  ]
})
export class ReadingModule {
}
