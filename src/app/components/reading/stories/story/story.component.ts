import {Component, OnInit} from '@angular/core';
import {StoryService} from '../../../../tools/services/story.service';
import {Story} from '../../../../tools/classes/Story';
import {ActivatedRoute, Router} from '@angular/router';
import {ChapterService} from '../../../../tools/services/chapter.service';
import {Chapter} from '../../../../tools/classes/Chapter';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.css']
})
export class StoryComponent implements OnInit {
  story: Story;
  chapters: Chapter[];

  constructor(private chapterService: ChapterService,
              private storyService: StoryService,
              private route: ActivatedRoute,
              private router: Router) {
    this.route.paramMap.subscribe(p => {
      if (p.get('id')) {
        this.storyService.getById(Number(p.get('id'))).subscribe( data => {
          this.story = data;
          console.log(data);
          this.chapterService.getAllActivesByStory(this.story).subscribe(c => {
            this.chapters = c.chapter;
            console.log(c);
          }, err => console.log(err));
        });
      } else {
        this.router.navigate(['/404']);
      }
    });
  }

  ngOnInit(): void {
  }
}
