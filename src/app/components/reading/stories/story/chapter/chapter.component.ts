import {Component, OnInit} from '@angular/core';
import {ChapterService} from '../../../../../tools/services/chapter.service';
import {ActivatedRoute, Router} from '@angular/router';
import {StoryService} from '../../../../../tools/services/story.service';
import {Story} from '../../../../../tools/classes/Story';
import {User} from '../../../../../tools/classes/User';
import {Chapter} from '../../../../../tools/classes/Chapter';

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.component.html',
  styleUrls: ['./chapter.component.css']
})
export class ChapterComponent implements OnInit {
  story: Story = new Story();
  user: User;
  chapter: Chapter = new Chapter();
  order: number;

  constructor(private chapterService: ChapterService,
              private storyService: StoryService,
              private route: ActivatedRoute,
              private router: Router) {
    this.route.paramMap.subscribe(p => {
      if (p.get('id')) {
        this.storyService.getById(Number(p.get('id'))).subscribe(data => {
          console.log(data);
          this.story = data;
          if (p.get('chapter')) {
            console.log('coucou');
            console.log(p.get('chapter'));
            this.order = Number(p.get('chapter'));
            this.getChapter();
          } else {
            this.order = 1;
          }
        });
      } else {
        this.router.navigate(['/404']);
      }

    });
  }

  ngOnInit(): void {
  }

  changeChapter(toto: number): void {
    this.order += toto;
    if (this.order >= 1) {
      this.getChapter();
    }
  }

  getChapter(): void {
    console.log('yo');
    this.chapterService.getOneByOrder(this.story.id, this.order).subscribe(chapter => {
      this.chapter = chapter.chapter;
      console.log(chapter);
    }, err => console.log(err));
  }

}
