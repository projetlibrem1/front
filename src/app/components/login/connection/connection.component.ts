import {Component, OnInit, Output} from '@angular/core';
import {AuthenticationService} from '../../../tools/services/authentication.service';
import {UserService} from '../../../tools/services/user.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {first} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SuccessHandler} from '../../../tools/handlers/success.handler';
import {ErrorHandler} from '../../../tools/handlers/error.handler';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.css']
})

export class ConnectionComponent implements OnInit {
  userForm: FormGroup;

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private router: Router,
    private successHandler: SuccessHandler,
    private errorHandler: ErrorHandler
  ) {
    if (this.authenticationService.isConnected()) {
      this.successHandler.handleSuccess('Tu es déjà connecté·e !');
      this.redirect();
    }
  }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      login: new FormControl('', [Validators.required, Validators.minLength(2)]),
      password: new FormControl('', [Validators.required])
    });
  }

  OnSubmit(): void {
    if (this.userForm.get('login').value !== '' && this.userForm.get('password').value !== '') {
      this.authenticationService.login(this.userForm.get('login').value, this.userForm.get('password').value).pipe(first())
        .subscribe((i_user) => {
            if (i_user) {
              console.log(i_user);
              this.successHandler.handleSuccess('Welcome Back ! :)');
              this.redirect();
            }
          },
          (err: HttpErrorResponse) => {
            console.log(err);
            this.errorHandler.handleError('Erreur d\'authentification ! ');
          });
    }
  }

  redirect(): void {
    if (this.authenticationService.isAdmin()) this.router.navigate(['/admin']);
    else this.router.navigate(['']);
  }

}
