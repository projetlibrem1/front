import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../tools/classes/User';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SuccessHandler} from '../../../tools/handlers/success.handler';
import {ErrorHandler} from '../../../tools/handlers/error.handler';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../tools/services/authentication.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  accountForm: FormGroup;
  // To re-user component for profile edition :
  @Input() selectedAccount?: User = new User();
  title: string;
  isUpdate = false;

  constructor(
    private userService: AuthenticationService,
    private fb: FormBuilder,
    private successHandler: SuccessHandler,
    private errorHandler: ErrorHandler,
    private router: Router

  ) {
  }

  ngOnInit(): void {
    this.title = this.selectedAccount.email !== undefined ? 'Profil' : 'Inscription';
    this.isUpdate = this.selectedAccount.email !== undefined;
    this.createForm();
  }

  createForm(): void {
    this.accountForm = new FormGroup({
      email: new FormControl(this.selectedAccount.email, [Validators.required, Validators.email]),
      name: new FormControl(this.selectedAccount.name, [Validators.minLength(3), Validators.maxLength(32), Validators.pattern(/[a-zA-ZàâæçéèêëîïôœùûüÿÀÂÆÇnÉÈÊËÎÏÔŒÙÛÜŸ_'-]+/)]),
      login: new FormControl(this.selectedAccount.login, [Validators.minLength(3), Validators.maxLength(32), Validators.pattern(/[a-zA-ZàâæçéèêëîïôœùûüÿÀÂÆÇnÉÈÊËÎÏÔŒÙÛÜŸ_'-]+/)]),
      password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[#?!@$%^&*-_]).{8,32}$/)]),
      password_validation: new FormControl('')
    });
    this.accountForm.setValidators(this.checkPasswords);
  }

  checkPasswords(group: FormGroup): object {
    const pass = group.get('password').value;
    const confirmPass = group.get('password_validation').value;
    return pass === confirmPass ? null : {notSame: true};
  }

  OnSubmit(): void {
    const newUserAccount = this.selectedAccount;
    newUserAccount.email = this.accountForm.get('email').value;
    newUserAccount.password = this.accountForm.get('password').value;
    newUserAccount.name = this.accountForm.get('name').value;
    newUserAccount.login = this.accountForm.get('login').value;
    this.userService.postUser(newUserAccount).subscribe(_ => {
      this.successHandler.handleSuccess(`Welcome ! Tu peux maintenant te connecter ! `);
      this.router.navigate(['/']);
    },
      error => {
        this.errorHandler.handleError('Oups, un problème est survenu !');
        console.log(error);
    });

  }

}
