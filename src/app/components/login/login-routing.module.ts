import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login.component';
import {ConnectionComponent} from './connection/connection.component';
import {InscriptionComponent} from './inscription/inscription.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
      // Here goes the paths of login children ( inscription, connexion, mp oublié... )
      // { path: 'monPath', component: MonComposant}
      {path: '', component: ConnectionComponent},
      {path: 'inscription', component: InscriptionComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
