import { Component, OnInit } from '@angular/core';
import {User} from '../../tools/classes/User';
import {UserService} from '../../tools/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = new User();

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

}
