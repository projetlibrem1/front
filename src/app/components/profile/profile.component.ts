import { Component, OnInit } from '@angular/core';
import {User} from '../../tools/classes/User';
import {AuthenticationService} from '../../tools/services/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User;

  constructor(private authService: AuthenticationService) {
    this.user = this.authService.getCurrent();
  }

  ngOnInit(): void {
  }

}
