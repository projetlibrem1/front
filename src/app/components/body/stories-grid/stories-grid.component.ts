import {Component, Input, OnInit} from '@angular/core';
import {Story} from '../../../tools/classes/Story';

@Component({
  selector: 'app-stories-grid',
  templateUrl: './stories-grid.component.html',
  styleUrls: ['./stories-grid.component.css']
})
export class StoriesGridComponent implements OnInit {
  @Input() stories: Story[];

  constructor() { }

  ngOnInit(): void {
  }


}
