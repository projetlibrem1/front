import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Theme} from '../../../tools/classes/Theme';
import {ThemeService} from '../../../tools/services/theme.service';
import {ErrorHandler} from '../../../tools/handlers/error.handler';
import {SuccessHandler} from '../../../tools/handlers/success.handler';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-theme-form-dialog',
  templateUrl: './theme-form-dialog.component.html',
  styleUrls: ['./theme-form-dialog.component.css']
})
export class ThemeFormDialogComponent implements OnInit {
  themeForm: FormGroup;
  theme: Theme;

  constructor(
    private themeService: ThemeService,
    private errorHandler: ErrorHandler,
    private successHandler: SuccessHandler,
    public dialogRef: MatDialogRef<ThemeFormDialogComponent>,

  ) { }

  ngOnInit(): void {
    this.themeForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(3)])
    });
  }



  OnSubmit(): void {
    this.theme = new Theme();
    this.theme.name = this.themeForm.get('name').value;
    this.themeService.create(this.theme).subscribe(d => {
      this.successHandler.handleSuccess('Theme créé avec succés !');
      this.dialogRef.close();
    });
    this.successHandler.handleSuccess('Theme créé avec succés !');
    this.dialogRef.close();
  }
}
