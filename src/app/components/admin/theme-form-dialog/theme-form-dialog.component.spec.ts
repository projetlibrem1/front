import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemeFormDialogComponent } from './theme-form-dialog.component';

describe('ThemeFormDialogComponent', () => {
  let component: ThemeFormDialogComponent;
  let fixture: ComponentFixture<ThemeFormDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThemeFormDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeFormDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
