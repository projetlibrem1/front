import { Component, OnInit } from '@angular/core';
import {StoryService} from '../../../tools/services/story.service';
import {Story} from '../../../tools/classes/Story';

@Component({
  selector: 'app-admin-stories',
  templateUrl: './admin-stories.component.html',
  styleUrls: ['./admin-stories.component.css']
})
export class AdminStoriesComponent implements OnInit {
  loaded = false;


  stories: Story[];
  constructor(
    private storyService: StoryService
  ) { }

  ngOnInit(): void {
    this.storyService.getAll().subscribe(data => {
      this.stories = data.stories;
      this.loaded = true;
    }, err => console.log(err));
  }

}
