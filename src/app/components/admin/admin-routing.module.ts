import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './admin.component';
import {AdminHomeComponent} from './admin-home/admin-home.component';
import {AdminThemesComponent} from './admin-themes/admin-themes.component';
import {AdminStoriesComponent} from './admin-stories/admin-stories.component';
import {AdminUsersComponent} from './admin-users/admin-users.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent, children: [
      // Here goes the admin paths ! :D
      // { path: 'monPath', component: MonComposant}
      { path: '', component: AdminHomeComponent},
      { path: 'themes', component: AdminThemesComponent},
      { path: 'stories', component: AdminStoriesComponent},
      { path: 'users', component: AdminUsersComponent},

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
