import { Component, OnInit } from '@angular/core';
import {User} from '../../../tools/classes/User';
import {UserService} from '../../../tools/services/user.service';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {

  loaded = false;
  users: User[];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.getAll().subscribe(d => {
      console.log(d);
      this.users = d.users;
      this.loaded = true;
    });
  }

}
