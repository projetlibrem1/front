import { Component, OnInit } from '@angular/core';
import {ThemeService} from '../../../tools/services/theme.service';
import {Theme} from '../../../tools/classes/Theme';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ThemeFormDialogComponent} from '../theme-form-dialog/theme-form-dialog.component';

@Component({
  selector: 'app-admin-themes',
  templateUrl: './admin-themes.component.html',
  styleUrls: ['./admin-themes.component.css']
})
export class AdminThemesComponent implements OnInit {
  loaded = false;
  themes: Theme[];

  constructor(
    private themeService: ThemeService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.themeService.getAll().subscribe(data => {
      console.log(data);
      this.themes = data.themes;
      this.loaded = true;
    });
  }

  openDialog(): void {
    const dial = this.dialog.open(ThemeFormDialogComponent, {
      width: '50%'
    });
    dial.afterClosed().subscribe(_ => this.ngOnInit());
  }
}
