import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AdminStoriesComponent } from './admin-stories/admin-stories.component';
import { AdminThemesComponent } from './admin-themes/admin-themes.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminTableComponent } from './admin-table/admin-table.component';
import {MatTableModule} from '@angular/material/table';
import { ThemeFormDialogComponent } from './theme-form-dialog/theme-form-dialog.component';
import { UserFormDialogComponent } from './user-form-dialog/user-form-dialog.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [AdminComponent, AdminStoriesComponent, AdminThemesComponent, AdminUsersComponent,
    AdminHomeComponent, AdminTableComponent, ThemeFormDialogComponent, UserFormDialogComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule
  ]
})
export class AdminModule { }
