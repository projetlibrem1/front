import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-admin-table',
  templateUrl: './admin-table.component.html',
  styleUrls: ['./admin-table.component.css']
})
export class AdminTableComponent implements OnInit {

  dataTest = [
    {nom: 'toto', machin: 'bidule', truc: 'chose'},
    {nom: 'toto2', machin: 'bidule', truc: 'chose'},
    {nom: 'toto3', machin: 'bidule', truc: 'chose'},
    {nom: 'toto4', machin: 'bidule', truc: 'chose'},
    {nom: 'toto5', machin: 'bidule', truc: 'chose'}
  ];
  rowDef = [];

  columns = [];

  @Input() title: string | 'Coucou';
  @Input() data: object[];

  constructor() { }

  ngOnInit(): void {
    console.log('coucou');
    this.initiateColumns();
  }

  // tslint:disable-next-line:typedef
  initiateColumns() {
    // tslint:disable-next-line:forin
    for (const key in this.data[0]) {
      console.log(key);
      console.log(this.data[key]);
      if (key !== 'password') {
        if (typeof this.data[0][key] === 'object') {
          console.log('in object');
          if (this.data[0][key] && this.data[0][key].hasOwnProperty('login')) {
            console.log('in login');
            this.columns.push({
              name: key.toString().charAt(0).toUpperCase() + key.slice(1),
              value: key,
              valueB : 'login'
            });
            this.rowDef.push(key);
          } else if (this.data[0][key] && this.data[0][key].name !== null) {
            this.columns.push({
              name: key.toString().charAt(0).toUpperCase() + key.slice(1),
              value: key,
              valueB: 'name'
            });
            this.rowDef.push(key);
          } else if (this.data[0][key] && this.data[0][key]?.title !== null) {
            this.columns.push({
              name: key.toString().charAt(0).toUpperCase() + key.slice(1),
              value: key,
              valueB: 'title'
            });
            this.rowDef.push(key);
          }
        } else {
          this.columns.push({
            name: key.toString().charAt(0).toUpperCase() + key.slice(1),
            value: key,
            valueB: null
          });
          this.rowDef.push(key);
        }


      }
    }
    console.log(this.rowDef);
    console.log(this.columns);

    this.columns.forEach(col => this.data[0][col.value]);
  }

}
