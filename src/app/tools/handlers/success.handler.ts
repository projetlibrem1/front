import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable()
export class SuccessHandler {
  constructor(
    public snackbar: MatSnackBar,
  ) {}

  public handleSuccess(mess: string): void {
    this.snackbar.open(mess, 'close', {
      panelClass: 'style-success',
      duration: 2000
    });
  }
}
