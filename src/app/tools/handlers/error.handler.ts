import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable()
export class ErrorHandler {
  constructor(
    public snackbar: MatSnackBar,
  ) {}

  public handleError(mess: string): void {
    this.snackbar.open(mess, 'close', {
      panelClass: 'style-error',
      duration: 2000
    });
  }
}
