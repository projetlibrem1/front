import {Story} from './Story';

export class Chapter {
  chapter_name: string;
  story: Story;
  text: string;
  id: number;
  story_id: number;
  status: string;
  order: number;
}
