import {User} from './User';
import {Story} from './Story';

export class Comments {
  user: User;
  story: Story;
  comment: string;
  id: number;
}
