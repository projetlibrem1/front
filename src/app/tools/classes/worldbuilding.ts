import {User} from './User';
import {Story} from './Story';

export class Worldbuilding {
  worldbuilding_id: number;
  story: Story;
  fiche_lieu: string;
  fiche_monde: string;
  fiche_perso: string;
}
