import {User} from './User';
import {Theme} from './Theme';

export class Story {
  title: string;
  average_rate: number;
  id: number;
  user: User;
  theme: Theme;
  theme_id: number;
  isTriggered: boolean;
  status: string;
  userId: number;
}
