import {User} from './User';
import {Story} from './Story';

export class Favorite {
  user: User;
  story: Story;
}
