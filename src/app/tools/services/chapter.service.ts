import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SuccessHandler} from '../handlers/success.handler';
import {ErrorHandler} from '../handlers/error.handler';
import {API_URL} from '../consts/API-const';
import {Story} from '../classes/Story';
import {Chapter} from '../classes/Chapter';

@Injectable({
  providedIn: 'root'
})
export class ChapterService {
  url = API_URL;
  headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  };

  constructor(
    private http: HttpClient,
    private successHandler: SuccessHandler,
    private errorHander: ErrorHandler
  ) {
  }

  create(chapter): any {
    return this.http.post(this.url + 'create/chapter', chapter, {
      headers: this.headers
    });
  }

  update(chapter): any {
    return this.http.put(`${this.url}chapter/${chapter.id}`, chapter);
  }

  getAllByStory(story: Story | number): any {
    story = (typeof story === 'number') ? story : story.id;
    return this.http.get<Chapter[]>(this.url + 'chapters/story/' + story);
  }

  getAllActivesByStory(story: Story | number): any {
    story = (typeof story === 'number') ? story : story.id;
    return this.http.get<Chapter[]>(this.url + 'chapters/active/' + story);
  }

  getOneByOrder(story: Story | number, order = 1): any {
    story = (typeof story === 'number') ? story : story.id;
    console.log(this.url + `chapters/order/${story}/${order}`);
    return this.http.get<Chapter[]>(this.url + `chapters/order/${story}/${order}`);
  }


  getById(chapter: Chapter | number): any{
    chapter = (typeof chapter === 'number') ? chapter : chapter.id;
    return this.http.get(`${this.url}chapters/${chapter}`);
  }

  getByOrder(chapter: Chapter | number): any {
    chapter = (typeof chapter === 'number') ? chapter : chapter.order;
    return this.http.get(`${this.url}chapters/${chapter}`);
  }

  delete(chapter: Chapter | number): any {
    chapter = (typeof chapter === 'number') ? chapter : chapter.id;
    return this.http.delete(`${this.url}/${chapter}`);
  }
}
