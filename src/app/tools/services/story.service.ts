import {Injectable} from '@angular/core';
import {API_URL} from '../consts/API-const';
import {Observable} from 'rxjs';
import {Story} from '../classes/Story';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AuthenticationService} from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class StoryService {
  url = API_URL + 'stories/';
  headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  };

  constructor(
    private http: HttpClient,
    private authService: AuthenticationService
  ) { }

  save(story): any {
    story.user_id =  this.authService.getCurrent().id;
    return this.http.post(`${API_URL}story`, story, {
      headers: this.headers
    });
  }

  getAll(): any {
    return this.http.get<any>(this.url, {
      headers: this.headers
    });
  }

  getLast(): any {
    console.log(this.url);
    return this.http.get(this.url + 'recent/posted', {
      headers: this.headers
    });
  }

  getById(id: number): any {
    console.log('get story by id : ', id);
    return this.http.get(`${API_URL}story/${id}`);

  }

  getAllByUser(id: number): any {
    return this.http.get(`${this.url}author/${id}`);
  }

  getAllActive(): any {
    return this.http.get(`${this.url}active`);
  }
}
