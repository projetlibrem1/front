import {Injectable} from '@angular/core';
import {API_URL} from '../consts/API-const';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '../classes/User';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private url = API_URL + 'api/auth/';
  headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  };

  constructor(private http: HttpClient,
              private userService: UserService
  ) {
  }

  postUser(user: User): Observable<any> {
    // console.log('ggiluhouhouhohoho');
    return this.http.post(this.url + 'signup', user, {headers: this.headers});
  }


  login(email: string, password: string): Observable<any> {
    return this.http.post<any>(`${this.url}signin`, {
      email,
      password
    }, {
      headers: this.headers
    })
      .pipe(map(i_user => {
        localStorage.setItem('user', JSON.stringify(i_user));
        localStorage.setItem('token', i_user.accessToken);
        return i_user;
      }));
  }

  isConnected(): boolean {
    return localStorage.hasOwnProperty('token');
  }

  getCurrent(): User {
    if (localStorage.hasOwnProperty('user'))
      return JSON.parse(localStorage.getItem('user'));
    else
      return null;
  }

  isAdmin(): boolean {
    if (this.getCurrent() !== null) {
      return this.getCurrent().roles.includes('ROLE_ADMIN');
    }
    return false;
  }

  logout(): void {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
  }
}
