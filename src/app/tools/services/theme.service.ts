import { Injectable } from '@angular/core';
import {SuccessHandler} from '../handlers/success.handler';
import {ErrorHandler} from '../handlers/error.handler';
import {API_URL} from '../consts/API-const';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Theme} from '../classes/Theme';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  url = API_URL + 'themes';
  headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  };

  constructor(
    private http: HttpClient,
    successHandler: SuccessHandler,
    errorHandler: ErrorHandler
  ) { }

  create(theme): any {
    return this.http.post(`${API_URL}create/theme`, theme, {
      headers: this.headers
    });
  }

  getAll(): any {
    return this.http.get<Theme[]>(this.url);
  }

  getById(theme: Theme | number): any {
    theme = (typeof theme === 'number') ?  theme  : theme.theme_id;
    return this.http.get<Theme>(this.url, {
      params : new HttpParams()
        .set('id', String(theme))
    });

  }

  delete(theme: Theme | number): any {
    theme = (typeof theme === 'number') ?  theme  : theme.theme_id;
    return this.http.delete(this.url, {
      params : new HttpParams()
        .set('id', String(theme))
    } );
  }
}
