import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {API_URL} from '../consts/API-const';
import {User} from '../classes/User';
import {AuthenticationService} from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  };
  url = API_URL + 'api/users';

  constructor(
    private http: HttpClient,
  ) { }

  getUserById( id: number): Observable<User> {
    return this.http.get<User>(this.url, {
      params: { id: id.toString() },
      headers: this.headers
    });
  }


  getAll(): any {
    return this.http.get(this.url);
  }
}
