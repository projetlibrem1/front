# Projet AcLab Master 1 MIII
Ce repository est le *front* de l'application. 
Le groupe se constitue de :
* Chiara Brichot (@Mid0na / GitLab)
* Thais Révillon (@Thaisr / GitLab)
* Adrien Lecomte (@lecomtea / GitLab)
* Jean-François Gautreau (@jeff59550 / GitLab)
* Ahlem Fahem (@hallouma.fahem1 / GitLab)
* Lamya Rayess (@lamya-rey / GitLab)

## Construction des modules : 
L'application est divisée en sous modules : 
 <ul>
    <li> Login : path /login </li>
    <li> Admin : path /admin</li>
    <li>Reading ( home se situe là ) : path racine</li>
    <li>Writing : path /creation</li>
    <li>Profile : path /profil</li>
 </ul>
 
## Development server

Run `ng serve` for a dev server.

## Build

Run `ng build` to build the project.

